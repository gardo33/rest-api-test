﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows.Navigation;
using System.Windows.Shapes;



//Verbindet auf die seite der REST API 
public class apiHelper
{
     public static HttpClient ApiClient { get; set; }
    public static void initionalizeClient()
    {
        ApiClient = new HttpClient();
        ApiClient.BaseAddress = new Uri("http://xkcd.com/");
        ApiClient.DefaultRequestHeaders.Accept.Clear();
        ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    }
}
